"""empty message

Revision ID: 5c301ec6211f
Revises: 77ff4ce9f507
Create Date: 2022-02-02 09:03:34.937777

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5c301ec6211f'
down_revision = '77ff4ce9f507'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index(op.f('ix_absent_day_date'), 'absent_day', ['date'], unique=False)
    op.create_index(op.f('ix_time_log_date'), 'time_log', ['date'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_time_log_date'), table_name='time_log')
    op.drop_index(op.f('ix_absent_day_date'), table_name='absent_day')
    # ### end Alembic commands ###
