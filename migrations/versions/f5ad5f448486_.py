"""empty message

Revision ID: f5ad5f448486
Revises: 90bca00d47b3
Create Date: 2021-11-19 16:47:38.415879

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f5ad5f448486'
down_revision = '90bca00d47b3'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('alter table grading alter column objective_scores type float[]')


def downgrade():
    op.execute('alter table grading alter column objective_scores type integer[]')
