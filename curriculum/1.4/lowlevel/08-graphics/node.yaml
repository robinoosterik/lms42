name: Graphics
description: Create real-time graphical animations.
goals:
    graphics: 1
    performance: 1
assignment:
    Get an image on the screen:
        ^merge: feature
        text: |
            <img class="side" src="mode0.png">

            Study `main.rs`. Complete the `Image::load` method and implement the `mode==0` case in the main loop, displaying `image1` without rotation and without repeating it.

            *Hint*: For each pixel (`y`,`x`) in `image1`, set the corresponding (`y`,`x`) in the output `buffer` (the screen) to that value. To calculate the array offset for a given pixel (`y`,`x`), you'll need to multiply the `y` coordinate with the width of the buffer or image you're calculating the offset for, and then just add `x` to it. So given an image with width 100, pixel (8,17) is located at array offset `8*100 + 17 = 817`.
            
    Rotate a single image:
        ^merge: feature
        text: |
            <aside class="side">
                <video src="mode1.mp4" autoplay muted loop></video>
                <p>The sudden jump in rotation is due to the video looping.</p>
            </aside>

            In the `mode==1` case, we want `image1` to rotate and repeat to fill the screen.

            The formula for rotating a coordinate (x: f32, y: f32) around (0,0) is:
            ```rust
            rotated_x = x * rotation.cos() + y * rotation.sin(),
            rotated_y = x * rotation.sin() - y * rotation.cos(),
            ```

            The way to approach this is to loop over all the pixels (`y`, `x`) on the output `buffer` (the screen), instead of looping over all pixels in the image as we did earlier. For each output buffer pixel, we'll want to calculate which image pixel it should copy by apply the rotation to the x and y coordinates (after casting them to `float`). Note that the resulting coordinates may be negative, or larger than the size of the source image. They need to *wrap around*. You can cast your coordinate to a *signed* integer and then use [rem_euclid](https://doc.rust-lang.org/std/primitive.i32.html#method.rem_euclid) for this.

    Rotate two superimposed images:
        ^merge: feature
        code: 0
        text: |
            <aside class="side">
                <video src="mode2.mp4" autoplay muted loop></video>
                <p>The sudden jump in rotation is due to the video looping.</p>
            </aside>

            In the `mode==2` case, we want `image1` to rotate clockwise, while `image2` is superimposed (at 50% transparency) while rotating counter-clockwise.

    "Make it fly!":
        1: Framerate with `mode==2` at 50% (or higher) of the provided `teacher-solution`.
        2: Framerate with `mode==2` at 65% (or higher) of the provided `teacher-solution`.
        3: Framerate with `mode==2` at 80% (or higher) of the provided `teacher-solution`.
        4: Framerate with `mode==2` at 90% (or higher) of the provided `teacher-solution`.
        text: |
            Optimize the `mode==2` case to achieve the highest frame rate you can.

            A few hints:

            - When working on performance, make sure you use `cargo run --release`, as release mode can be tens of times faster than debug mode. When your program doesn't behave as expected, always switch back to debug mode, as it will give more and more detailed error messages.
            - Focus your optimization on the most inner loop; this is where the CPU will likely be spending most of its time. Try to move as many computations as you can to *outside* of this inner loop.
            - Always measure the performance difference after implementing what you hope is an optimization. Don't just guess. Watch the FPS that is logged to the console.
            - Operations like multiplication and (even more so) division take longer than addition and subtraction. Can you optimize them away from the inner loop?
            - When superimposing, can you work on the whole pixel `u32` at a time, instead of per color channel?

            If code quality *necessarily* suffers a little due to an optimization, that will *not* be counted negatively towards to *Code quality* objective if you properly explain the compromise in comments.

