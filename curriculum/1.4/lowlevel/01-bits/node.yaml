name: Gates, bits, bytes, numbers
description: How does a computer work on the lowest level?
goals:
    bindata: 1
    computerarchitecture: 1
days: 2

resources:
    Logic gates:
    -
        link: https://www.youtube.com/watch?v=QZwneRb-zqA
        title: Exploring How Computers Work
        info: This video does a really good job of explaining how a computer can add or subtract two numbers, building up from just electric wires and switches. Watching this fully is highly recommended.
    -
        link: http://nandgame.com/
        title: Nandgame
        info: An online interactive environment to play with logic gates to build your own simulated computer. We recommend you take say half an hour to complete as many lessons as you can, to get a good feel for digital logic. Of course, if you're hooked on the game, you're highly encouraged to continue playing it some other time.

    Bits, bytes and numbers:
    -
        link: https://www.youtube.com/watch?v=I0-izyq6q5s&list=PLFt_AvWsXl0dPhqVsKt1Ni_46ARyiCGSq&index=2
        title: How Do Computers Remember?
        info: This second video in the 'Exploring How Computers Work' series continuous by creating a memory cell, allowing the results of a computation to be stored and used for further computations. Along the way, we'll also learn why your computer's CPU has a clock rate (something like 2.6 GHz) and what that actually means. Don't worry if you don't fully understand how the gate logic works.
    -
        link: https://www.youtube.com/watch?v=USCBCmwMCDA&list=PLzdnOPI1iJNcsRwJhvksEo1tJqjIqWbN-&index=3
        title: "How Computers Work: Binary & Data"
        info: Explains how bits combine to bytes, and how they are used to store useful stuff like text, sound and video.
    -
        link: https://www.youtube.com/watch?v=dPxCGlW9lfM
        title: Why do programmers use hexadecimal numbers?
        info: What are hexadecimal numbers and how and why are they used?
    -
        link: https://www.youtube.com/watch?v=BtlmTAijcOQ
        title: "Godot - Endianness: Big and Little Endian"
        info: Computers differ in the order that they combine bytes into larger numbers. This video explains why and how.


    Working with binary data:
    - To inspect binary files in Visual Studio Code, install the *Hex Editor* extension. When it is installed, you can right-click on a filename, select 'Open with...' and then choose the Hex Editor. It has a dropdown box to select endianness.
    -
        link: https://www.youtube.com/watch?v=qnKX1y7HAyE&t=286s
        title: Bytes and Bytearray tutorial in Python 3
        info: In Python `bytes` and `bytearray` can be used to work with binary data. The video shows how. You can probably go over it quickly.
    -
        link: https://www.youtube.com/watch?v=frwqnS9ICxw
        title: Python Programming Tutorial - 51 - Bitwise Operators
        info: When we're interested in one or multiple bits in a byte (as opposed to the whole byte), we can use the Python bitwise operators to get to them. This video explains it.
    -
        link: https://realpython.com/python-bitwise-operators/
        title: Bitwise Operators in Python
        info: In case the previous video doesn't do the trick for you, this text gives a pretty comprehensive view of the bitwise operators in Python. In particular, we'd recommend you study the sections about *Bitwise Logical Operators* and *Bitwise Shift Operators*.

assignment:
    BitReader:
        - |
            In this first part of the assignment, you'll implement the `BitReader` class, that allows its users to read single bits or integers of any number of bits from a stream of binary data.

        -
            title: get_bit
            ^merge: feature
            text: |
                Implement the `get_bit()` method in the `bit_reader.py` file, which should read and return a single bit from the input stream.

                The file `bit_reader_test.py` provides a unit test. The first test exercises the `get_bit()` method. It is worth studying, as it makes it really clear what is expected. Also, make sure the first test passes without issue before moving on to the next objective.

        -
            title: get_number
            ^merge: feature
            text: |
                Implement the `get_number(bit_count)` method in the `bit_reader.py` file, which should read `bit_count` bits from the input stream and return it as an unsigned number. You should *not* have to use strings or lists in order to accomplish this, but instead directly modify an integer variable based on the bits that you're reading (using `get_bit()`).

                Make sure the second test passes before moving on to the next objective.

        -
            link: https://www.youtube.com/watch?v=sEqbPvBwwW8
            title: An introduction to 2's Complement (Negative Numbers)
        -
            title: get_signed_number
            ^merge: feature
            text: |
                Implement the `get_signed_number()` method in the `bit_reader.py` file, which should read `bit_count` bits from the input stream, interpret it as a Two's Complement binary number, and return it as a signed integer. Again, strings or lists should *not* be used.

                Make sure the entire unit test passes before moving on to the next part of the assignment.

    Img42:
        - |
            We have invented our own little file format for storing and losslessly compressing images! Sure, it doesn't compress as well as, say PNG, but it's got 42 in the name, so it's cooler.

            The file format is relatively simple:

            - Files must start with our magic marker, the 5 byte ASCII string 'Img42'.
            - Next is the **width** of the image is pixels. It is a 16 bit unsigned integer.
            - Next the **height** of the image in pixels. Same encoding.
            - For each of the width*height pixels in the image, we need to output the 24-bit color (1 byte for red, 1 byte for green, 1 byte for blue). The pixels are ordered from left to right, from top to bottom. So we'll start with the top row, then move to the row just below that, etc. For each pixel there's:
                - A **copy bit**. If it is 1, the color of this pixel should be identical to the previous pixel (the one to the left, or when we're working on the left-most pixel of a line, the previous pixel would be the right-most pixel of the line above). 
                - If the *copy bit* was **not** set, then for each of the three channels (red, green and blue):
                    - A **relative bit**.
                    - If the *relative bit* is set to 1:
                        - A 3-bit signed integer (so ranging from -4 to 3 inclusive). This value should be added to the previous pixel value for this channel to get the new value.
                    - If the *relative bit* is set to 0:
                        - An 8-bit unsigned integer. This is the value for this channel.
                
            The file format uses **Big Endian** byte ordering (as does `BitReader`, if you implemented it correctly).

            So let's analyse a simple 2x2 example image. In the data types below `uint16` is short for a 16 bit unsigned integer, while `int3` is short for a 3 bit signed integer.

            ```
            ===== Header =====

                   data:   01001001 01101101 01100111 00110100 00110010   00000000 00000010   00000000 00000010
                           --------------------------------------------   -----------------   -----------------
              data type:   5 byte string                                  uint16              uint16
            description:   magic marker                                   width               height
                  value:   ("Img42")                                      (2)                 (2)


            ===== Pixel 0 (x=0 y=0 colors=(128,255,0)) =====

                   data:   0          0          10000000   0          11111111      0          00000000
                           --------   --------   --------   --------   -----------   --------   --------
              data type:   bit        bit        uint8      bit        uint8         bit        uint8
            description:   copy?      relative?  red        relative?  green         relative?  blue
                  value:   (off)      (off)      (128)      (off)      (255)         (off)      (0)


            ===== Pixel 1 (x=1 y=0 colors=(128,255,0)) =====

                   data:   1
                           --------
              data type:   bit
            description:   copy? 
                  value:   (on)


            ===== Pixel 2 (x=0 y=1 colors=(130,255,50)) =====

                   data:   0          1          010     1          000     0          00110010
                           --------   --------   -----   --------   -----   --------   --------
              data type:   bit        bit        int3    bit        int3    bit        uint8
            description:   copy?      relative?  red     relative?  green   relative?  blue
                  value:   (off)      (on)       (+2)    (on)       (+0)    (off)      (50)


            ===== Pixel 2 (x=1 y=1 colors=(129,251,70)) =====

                   data:   0          1          111     1          100      0         01000110
                           --------   --------   -----   --------   -----   --------   --------
              data type:   bit        bit        int3    bit        int3    bit        uint8
            description:   copy?      relative?  red     relative?  green   relative?  blue
                  value:   (off)      (on)       (-1)    (on)       (-4)    (off)      (70)

            ```

            The compression works based on the assumption that nearby pixels often have (approximately) the same color. If a pixel is exactly the same as the previous pixel, that pixel can be stored using just 1 bit (instead of the 24 bits you'd normally require for 8-bit red, 8-bit green and 8-bit blue). Failing that, the *relative* mechanism often allows storing a pixel in just 13 bits (1 copy bit and 3 times a relative bit plus a 3-bit relative color), if the color is close to the previous pixel. If that also doesn't work (for none of the red, green and blue channels), the 'compressed' output will be a bit larger than the naive storage of this pixel: 28 bits instead of 24 bits.

    
        -
            title: decode(data) function
            ^merge: feature
            weight: 2
            text: |
                An encoder (compressor) for the Img42 format has been written. In the template you'll find three Img42 example images (`test[123].img42`).

                Unfortunately, what's still lacking is a viewer for these files. That's where you come in! 😀

                Implement the `decode(data)` function in `viewer.py`. Once implemented, you should be able to view a file by typing something like `python viewer.py test1.img42' at the command prompt.

                Decoding a large file will be pretty slow. We'll address that in the *Low-level programming* module, working with the Rust language. For now, just see if you can get your decoder to run on the largest file (`test3.img42`) in under 15s.

                **Note:** Use *poetry* to make sure Python has access to the library (`Pillow`) required for displaying the resulting image.
