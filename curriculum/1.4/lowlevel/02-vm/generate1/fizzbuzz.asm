sp: int16 stack

arg1: int16 0
cnt: int16 0
tmp1: int16 0

fizz: "Fizz\0"
buzz: "Buzz\0"
fizzBuzz: "Fizz Buzz\0"
comma: ", \0"

macro call FUNCTION {
	set16 **sp *ip # Copy the address of this instruction into **sp
	add16 **sp 24 # skip the 24 bytes for all instruction in this macro
	add16 *sp 2 # Increment the stack pointer
	set16 *ip FUNCTION
}

macro return {
	sub16 *sp 2
	set16 *ip **sp
}

# destroys: tmp1
macro ifdiv X Y {
	set16 *tmp1 X
	div16 *tmp1 Y
	mul16 *tmp1 Y
	if=16 *tmp1 X
}

main:
	set16 *cnt 1
again:
	ifdiv *cnt 3
	set16 *ip printFizz
	
	ifdiv *cnt 5
	set16 *ip printBuzz

	set16 *arg1 *cnt
	call printNumber
	# fall through
	
next:
	add16 *cnt 1
	if>16 *cnt 100 set16 *ip end
	set16 *arg1 comma
	call printString
	set16 *ip again

printFizz:
	ifdiv *cnt 15
	set16 *ip printFizzBuzz
	
	set16 *arg1 fizz
	call printString
	set16 *ip next
	
printBuzz:
	set16 *arg1 buzz
	call printString
	set16 *ip next
	
printFizzBuzz:
	set16 *arg1 fizzBuzz
	call printString
	set16 *ip next


	
end:
	set16 *ip 0
	

# printString
# in:
#   arg1: address of the zero-terminated string
# out:
#   arg1: points at the terminating zero
printString:
	if=8 **arg1 0 set16 *ip printStringExit
	write8 **arg1
	add16 *arg1 1
	set16 *ip printString
printStringExit:
	return


# printNumber
# in:
#   arg1: the number to print in decimal
# out:
#   arg1: will be 0
# destroys: tmp1, any stack with sp>=0
printNumber:
	# tmp1 will point at the next stack position to write a character to
	set16 *tmp1 *sp

printNumberCalcLoop:
	# *tmp1 = arg1 % 10
	set16 **tmp1 *arg1
	div16 *arg1 10
	mul16 *arg1 10
	sub16 **tmp1 *arg1

	# tmp1++
	add16 *tmp1 1

	# arg1 /= 10
	div16 *arg1 10

	# loop while arg1 > 0
	if>16 *arg1 0 set16 *ip printNumberCalcLoop
	
printNumberShowLoop:
	# write *--tmp1 + '0'
	sub16 *tmp1 1
	add8 **tmp1 48 # '0' in the asci table
	write8 **tmp1

	# loop while tmp1 > sp
	if>16 *tmp1 *sp set16 *ip printNumberShowLoop
	
	return

stack:
