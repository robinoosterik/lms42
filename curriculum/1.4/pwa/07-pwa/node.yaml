name: Progressive Web Applications
description: Build progressive web applications, featuring offline functionality as well as WebSocket real-time communication.
goals:
    pwa: 1
days: 2
intro:
    About the exam: |
        <div class="notification">
            The exam for this module consists of a project that requires you to implement something based on your own idea. We recommend that you come up with an idea in advance. The project requirements are available at any time.
        </div>


assignment:
    -
        title: Public URL
        must: true
        text: |
            Some of the web features you'll learn about in this assignment require your web app to be served securely (through `https`) from a publicly accessible URL. This also allows you to tests your app on devices other than your development computer.

            Developers often use commercial tools like [ngrok](https://ngrok.com/) to expose a local development server on a public `https` URL. We're providing our own (free) alternative to this in the template: *expose-http*. Assuming that your local `http` server is running on port 3000, you can create a public address by running:
            
            ```
            ./expose-http 3000
            ```

            The template also contains a Svelte project setup such that a single HTTP server (running on a single port), can serve the frontend, as well as the backend, as well as the real-time WebSocket server we'll be learning about. You can start it using:

            ```
            npm install && npm start
            ```

            Make sure you get the application up-and-running on a publicly accessible URL. You should be able to open it on your phone, and when pressing the *count is*-button, the count should instantly be incremented on all connected browsers.
    -
        title: Frontend
        ^merge: feature
        text: |
            Within the `frontend/` directory of the provided template, build a Svelte application that displays a user-interfaces consisting of these two screens.

            <wired-card style="width: 100%; display: flex; flex-direction: column;">
                <div>
                    <svg style="display: inline-block; width: 32px; height: 32px; vertical-align: bottom;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M112.31 268l40.36-68.69l34.65 59l-67.54 115h135L289.31 432H16zm58.57-99.76l33.27-56.67L392.44 432h-66.68zM222.67 80h66.59L496 432h-66.68z" fill-rule="evenodd" fill="currentColor"/></svg>
                    <h2 style="display: inline; margin-top: 0;">Login</h2>
                </div>
                <hr>
                <div style="display: flex; gap: 1em;">
                    <wired-input style="flex: 1; display: block;" placeholder="Your name..." rows="1.5"></wired-input>
                    <wired-button>Log in</wired-button>
                </div>
            </wired-card>

            <wired-card style="width: 100%; display: flex; flex-direction: column;">
                <div>
                    <svg style="display: inline-block; width: 32px; height: 32px; vertical-align: bottom;" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path d="M112.31 268l40.36-68.69l34.65 59l-67.54 115h135L289.31 432H16zm58.57-99.76l33.27-56.67L392.44 432h-66.68zM222.67 80h66.59L496 432h-66.68z" fill-rule="evenodd" fill="currentColor"/></svg>
                    <h2 style="display: inline; margin-top: 0;">Chat</h2>
                </div>
                <hr>
                <div style="display: flex; flex-direction: column; flex: 1;">
                    <div>
                        <wired-card style="margin-right: 33%;">
                            <b>Frank:</b> Does this actually work?
                        </wired-card>
                    </div>
                    <div>
                        <wired-card style="margin-left: 33%;">
                            <b>Timothy:</b> Of course not, this is just a mockup.
                        </wired-card>
                        <wired-card style="margin-left: 33%;">
                            <b>Ivo:</b> Or is it....!?
                        </wired-card>
                        <div style="text-align: right;">Timothy is typing...</div>
                        <div style="text-align: right;">Tibor is typing...</div>
                    </div>
                    <div style="flex: 1; min-height: 100px;"></div>
                </div>
                <wired-textarea style="display: block; width: auto;" placeholder="Your message..." rows="1.5"></wired-textarea>
            </wired-card>

            <script src="/static/wired-elements.js"></script>

            - After typing a username in the *login screen*, the *chat screen* should be shown.
            - The list of messages (and their senders) and the list of now-typing chat participants can just be hard-coded for now.
            - The application does not need to do anything yet, except displaying a message at the bottom list (with your user name as the sender) and clearing the textarea, when enter is pressed in the textarea.
            - Messages sent by your user name should be left-align. Other messages should be right-aligned.
            - The logo should be a PNG image, and the (basic) styling should be done from within an external CSS file.
            - When there are more messages than fit on the screen, the messages should be scrollable, leaving the header and the textarea in place.
            - You'll want to keep the WebSocket example in `Counter.svelte` around, as you'll need to do something similar later on.
            
    -
        link: https://www.youtube.com/watch?v=sFsRylCQblw
        title: Progressive Web Apps in 100 Seconds // Build a PWA from Scratch
        info: You can ignore the part about the Workbox library - we won't be using it.
    -
        link: https://www.youtube.com/watch?v=jVfXiv03y5c
        title: Introduction to Service Workers
        info: More info on how what Service Workers are and how you can use them.
    -
        link: https://developers.google.com/web/ilt/pwa/caching-files-with-service-worker
        title: Google Web Fundamentals - Caching Files with Service Worker
    -
        link: https://developers.google.com/web/ilt/pwa/tools-for-pwa-developers#interact_with_service_workers_in_the_browser
        title: Google Chrome - Tools for PWA developers - Interact with service workers in the browser
        info: |
            Although for most web development work Firefox is perfectly fine, when it comes to Service Workers the Google Chrome (or Chromium) developer tools are way ahead. If you don't have it yet, you can install `chromium` using Manjaro's *Add/remove Software*.

    -
        ^merge: feature
        title: Offline app
        text: |
            Make your web app fully available offline, using a custom Service Worker. 

            Requirements:

            - We want to employ a *Network falling back to the cache* strategy, meaning resources are only loaded from the cache if we're unable to load them from the network.
            - We *don't* want the service worker to have a full list of resources to be cached. Lists like tend to become outdated very quickly. Instead, all resources that have been retrieved while there was a proper network connection should be cached. Another benefit of doing this (instead of caching everything on `install`) is that the cache is updated to contain the most recent versions whenever we open the page while online.
            - Use JavaScript `async` and `await` to work with promises, instead of the old-style `then()` and `catch()` methods that you'll see in most documentation. This should make your service worker easier to understand.
            - Your service worker should *not* use any libraries such as Workbox.
            - Just copy/pasting an example from internet will not suffice. You need to understand what you're doing.
            
            You can test using the *Offline* checkbox (or *Network throttling &rarr; Offline*) in the developer tools. The browser reload button may not work as it sometimes clears caches. Instead, just press *enter* in your browser's URL bar to load the page again.

            Service Workers may not work on localhost (without `https`), so use the public URL provided by *expose-http* to test.

    -
        link: https://web.dev/add-manifest/
        title: web.dev - Add a web app manifest
        info: A well-explained example of a web app manifest.
    -
        ^merge: feature
        title: Installable app
        weight: 0.5
        text: |
            Make your static web site *installable* by adding a web app manifest. Test this on your mobile phone (through *expose-http*). Provide your own icon to use on the home screen. Tapping it should open the site as an 'app', meaning the browser controls (such as the URL bar) will not be shown.

    -
        link: https://sebhastian.com/local-storage-introduction/
        title: Introduction To Local Storage
        info: Local Storage is a way to easily store some data in the browser in such a way that it is still available on the user's next visit. Note that this standard is aimed at small amounts of data - in case you need to store more data, browsers offer the IndexedDB API, which is a bit more complex.
    
    -
        link: https://svelte.dev/tutorial/update
        title: Svelte tutorial - Lifecycle - beforeUpdate and afterUpdate
        info: A chat example in Svelte, with instruction on how you would automatically scroll to the bottom. 
    -
        ^merge: feature
        title: Persistent data
        weight: 1
        text: |
            Use Local Storage to make your app persist data, such that when the page is opened again, your user name and any messages you sent are still there. This should work even when the page is offline.

    -
        link: https://www.youtube.com/watch?v=1BfCnjr_Vjg
        title: WebSockets in 100 Seconds & Beyond with Socket.io
        info: Stop watching after the 100 seconds. We won't be using Socket.io nor WebRTC.
    -
        link: https://flaviocopes.com/node-websockets/
        title: Using WebSockets with Node.js
        info: The example shown here resembles the example in the video, but as this is text it can be a bit easier to study at your own pace.
    -
        link: https://github.com/websockets/ws/blob/master/doc/ws.md
        title: ws - Documentation
        info: The official API documentation for the `ws` package for Node.js.
    -
        ^merge: feature
        title: Real-time chat
        text: |
            Now turn your PWA into an actual chat app, by adding a WebSocket backend and exchanging messages with it.

            All messages should still be stored to Local Storage, so users can see the old messages while they're offline (and while your app is loading messages from the server). When there is no WebSocket connection yet, the textarea should be hidden. As soon as there's a connection, all up-to-date messages should be displayed and the textarea should be shown so that the user can send messages.

            When the WebSocket connection fails (due to the server/internet connection going down), the textarea should be hidden and the application should retry connecting every 2 seconds.

            The backend can just store all messages in-memory; no database needed this time.

    -
        ^merge: feature
        title: Typing notification
        weight: 0.5
        text: |
            Add working typing notification (and shown in the second wireframe) to your application, using WebSocket.

    - Before turning in, make sure you remove all spurious (template) code you didn't end up using.
