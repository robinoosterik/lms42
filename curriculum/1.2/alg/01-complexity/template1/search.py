import random
import time
import math


# These allow you to tweak how many needles there are to be found in how large a haystack.
# Make sure you put these back to 2000000 and 100 and to test with these values before you
# hand in your work.
haystack_size = 2000000
needle_count = 100


def linear_search(haystack, needles):
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """

    # TODO: implementation!
    return []

def dict_search(haystack, needles):
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """

    # TODO: implementation!
    return []

def binary_search(haystack, needles):
    """Returns a list of indexes, one for each of the needles in the haystack. If the same needle
    has multiple copies in the haystack, the index for just one of the copies will be put in the
    returned list. If a needle does not exist in the haystack, `None` is put in the returned list.

    Arguments:
        haystack {list} -- An ordered list of numbers to be searched in.
        needles {list} -- A list of numbers to search for in haystack.

    Time complexity: O(TODO) where h=len(haystack) and n=len(needles)
    """
    # TODO: implementation!
    return []


# Create a list of `haystack_size` ascending numbers (meaning each number is greater than or equal to the previous number).
haystack = []
next_number = 0
for _ in range(haystack_size):
    next_number += random.randint(0,3)
    haystack.append(next_number)

# Create a list of `needle_count` random numbers between the minimum and the maximum values in `haystack`.
# These will be the needles that we'll try to find in haystack. Note that about half the needles will not
# actually be present in the haystack.
needles = [random.randint(haystack[0], haystack[-1]) for _ in range(needle_count)]

# Loop over each of the three implementations.
for func in [linear_search, dict_search, binary_search]:
    print(f"Running {func.__name__}:")

    # First check a few edge cases.
    if func([], [1]) != [None]:
        print(f"  Error: wrong result for empty haystack")
    if func([1], [1]) != [0] or func([1], [2]) != [None]:
        print(f"  Error: wrong result for haystack with length 1")
    if func([1,2], [1]) != [0] or func([1,2], [2]) != [1] or func([1,2], [3]) != [None]:
        print(f"  Error: wrong result for haystack with length 2")

    # Run the implementation and store the before and after times, to calculate how long it took.
    start_time = time.perf_counter()
    results = func(haystack, needles)
    end_time = time.perf_counter()

    # Print any errors in the result returned by the implementation.
    if not isinstance(results, list):
        print(f"  Error: algorithm returns something other than a list")
    elif len(results) != len(needles):
        print(f"  Error: algorithm returns a list of size {len(results)} instead of {len(needles)}")
    else:
        for target_index, target_number in enumerate(needles):
            haystack_index = results[target_index]
            if haystack_index == None:
                if target_number in haystack:
                    print(f"  Error: algorithm incorrectly says that {target_number} is not in haystack")
            else:
                if haystack[haystack_index] != target_number:
                    print(f"  Error: algorithm says that {target_number} is at position {haystack_index} in the haystack, but that position contains {haystack[haystack_index]}")

    print(f"  Finished in {round(end_time - start_time, 3)}s")
