Assignment: |
    In this assignment you will build:
    1. your own object-oriented retro, text-mode game library, and
    2. two games that use your library.

    In order to help you understand what it is you'll be working towards, we'll start by introducing the games.

"Game 1: Rocket Game": |
    Here's what the first game should resemble:

    <link rel="stylesheet" type="text/css" href="/static/asciinema-player.css" />
    <script src="/static/asciinema-player.min.js"></script>

    <script>
        (function() {
            let el = document.createElement('div');
            document.currentScript.after(el);
            AsciinemaPlayer.create('rocketgame.cast', el, {loop: true, poster: 'npt:0:10'});
        })();
    </script>

    The game should have the following features:

    - The player should be able to move the rocket up and down using the arrow keys. The ship should not be allowed to move passed the end of the screen.
    - Asteroids should come hurling to the left towards the rocket at three different horizontal speeds, indicated by three different colors. Each asteroids should also have a random but low vertical speed. Asteroids can be either small or large. 
    - When the player presses space, a bullet is launched straight to the right. This can be done at most two times per second.
    - When a bullet hits an asteroids, the bullet disappears and the player's score gets incremented by one. If the asteroid was small, it disappears as well. If it was big, it splits up into two small asteroids, moving in slightly different directions.
    - The score is displayed in the top right corner.
    - When an asteroids hits the player, the game ends, displaying the total score.
    - The game should utilize the full size of the terminal. (And yes, this means the game is easier to play on larger screens.)

"Game 2: Block Jumper": |
    Here's what the first game should resemble:

    <script>
           (function(){
            let el = document.createElement('div');
            document.currentScript.after(el);
            AsciinemaPlayer.create('blockjumper.cast', el, {loop: true, poster: 'npt:0:10'});
        })();
    </script>

    The game should have the following features:

    - The player should be able to move the *jumper* in four directions using the arrow keys. The jumper should not be allowed to move passed the end of the screen.
    - The goal is to stay in the game for 90 seconds. A progress bar is shown at the bottom. Once it is full, the game ends, congratulating the player.
    - Blocks or arbitrary sizes (between 1 and 25 in both dimensions) are coming from the top, right, bottom and left. If a block hits the jumper, the width of the progress bar will be halved and the jumper will change appearance for two seconds and not be hittable again during that time.
    - When the player presses the space bar, the jumper will jump for one second, during which time it changes appearance and cannot be hit by any blocks. After landing, the jumper enters *recovery mode* for two seconds, during which it has yet another appearance and cannot jump (but *can* be hit).

Game library: |
    Create a game library that offers programmers an easy and object-oriented way to build retro-looking text-mode games.

    The library should...
    - Be very generic. It particular, nothing in it should be specific to our little rocket game.
    - Use two types of *Entity* objects to represent and draw on-screen objects.
        - Rectangles of arbitrary width and height, filled with a single character.
        - Multi-line strings that can be used to form shapes (as provided in the template games).
    - Contain the *game loop* that gathers keyboard input and updates and draws all entities 30 times per second.
    - Use `blessed` internally, but not expose any of `blessed` to the games created using this library.
    - Provides the games with a way to detect colliding entities. This only needs to be done based on their *bounding boxes* (the smallest possible rectangle you could draw around a shape).
    - *Not* be much slower than it needs to be. We don't expect stellar performance though.


Class diagram: |
    Here's a high-level class diagram for game 2 and the library. You're not required to follow it, but if you come up with your own class design, we do expect it to be at least as well-designed as this one.

    ```plantuml
    package blessed {
        class Terminal {}
    }
    package retrogamelib {
        class Game {
            run()
            get_entities_of_type(type): list[Entity]
            was_key_pressed(key): bool
            ...
        }
        Game --> Terminal
        abstract class Entity {
            +x: float
            +y: float
            +color: str
            {abstract} +update(game: Game)
            {abstract} +draw(terminal: Terminal)
            +collides_with(other: Entity): bool
            {abstract} +get_size(): tuple(int,int)
        }
        note left {
            Note that while it is useful to store
            x and y as floats (in order to allow
            gradual movement) you will need to
            round() them to int in draw().
        }
        class ShapeEntity {
            +shape: str
            +draw(terminal)
            +get_size(): tuple(int,int)
        }
        class RectangleEntity {
            -width: int
            -height: int
            +character: str
            +draw(terminal)
            +get_size(): tuple(int,int)
            +set_size(width: int, height: int)
        }
        ShapeEntity --|> Entity
        RectangleEntity --|> Entity

        Game --> "0..*" Entity
    }

    package blockjumper {
        class BlockJumperGame {
            +get_score(): int
        }
        BlockJumperGame ---|> Game

        class Jumper {
            -state: str
            -state_time: int
            +update(game)
        }
        Jumper ---|> ShapeEntity

        class Block {
            +update(game)
        }
        Block ---|> RectangleEntity

        class ScoreProgress {
            +update(game)
        }
        ScoreProgress ---|> RectangleEntity
    }
    ```

Objectives:
-
    title: Library functionality, convenience and flexibility
    text: |
        The library should provide all (or as much as you can) of the functionality specified above and required by the rocket game. It should be easy to use and flexible, such that programmers can quickly create all kinds of text-mode games..

        *Hints:*

        - Use [blessed's full-screen and cbreak modes](https://blessed.readthedocs.io/en/latest/terminal.html#full-screen-mode). The latter causes keys to be read directly after they are typed, as opposed to a line at a time when enter is pressed, which is the regular terminal behavior.
        - To improve performance and to prevent scrolling when printing something on the bottom line, always use `print(something, end='')`. When everything is drawn, make sure that nothing is left in `print`'s output buffer (which is usually flushed on every newline) by flushing it using `print('', end='', flush=True)`.
        - Make sure you don't `print` any characters that don't fit in the terminal window, as they would wrap to the next line.
        - Use [blessed's inkey](https://blessed.readthedocs.io/en/latest/keyboard.html#inkey) to wait for a keystroke with a timeout. In order to run at 30 frames/second, you'll need to wait for 1/30s in total, meaning you'll have to restart `inkey` with the remainder of the timeout when a key is pressed. If no key was pressed before the timeout, `inkey` returns `None`. Otherwise it will return a `key`, which can be converted to a usable string like this: `key_str = key.name or str(key)`.

    1: 25% of features implemented in a generic and easy to use way.
    2: 50% ...
    3: 75% ...
    4: 100% ...
    map:
        libraries: 1
        abstraction: 1
        decomposition: 1
        class-diagram: 1
        inheritance: 1
        static: 1

-
    title: Game 1
    ^merge: feature
    code: 0
    text: |
        The game should work as specified and shown in the video above.
    map:
        refactor: 1
        classes: 1
        class-diagram: 1
        static: 1
-
    title: Game 2
    ^merge: feature
    code: 0
    text: |
        The game should work as specified and shown in the video above.
    map:
        refactor: 1
        classes: 1
        class-diagram: 1
        inheritance: 1
        static: 1
-
    ^merge: codequality
    map:
        libraries: 0.5
        abstraction: 0.5
        decomposition: 0.5
        class-diagram: 0.5
        inheritance: 0.5
        static: 0.5
        refactor: 0.5
        classes: 0.5
    text: |
        OOP code quality guidelines:
        - Only use *private* attributes (with double underscores) unless there's a good reason to make attributes (semi) public. Superclasses should never use attributes initialized by subclasses though.
        - Methods that are only to be used internally by the class itself, should also be considered *private*.
        - Interact with instances of other classes not by reading/writing their attributes, but by calling a method, nicely asking the class to do something for you.
        - Put functionality in a method of the class that it (mostly) concerns. If it concerns multiple classes, see if you can split up the functionality. If it is related to a class, but doesn't use any instance attributes, make it a static method.
        - Try to prevent creating (too many) setters and getters, as they imply the actual functionality acting on this class is being implemented outside of the class.
        - Use abstract classes and abstract methods instead of *duck typing*.

-
    title: Documentation
    map: inline-doc
    text: |
        Create HTML documentation for your library (*not* the games) using `pdoc3`. All *public* methods and attributes (those that are meant to be accessed by games) should be documented such that a developer new to the library should be able to implement a game without looking at `gamelib.py` or any examples. Don't forget to explain the meaning of any method arguments and return values.
    0: The docs are useless / absent.
    2: The docs are helpful, but one would frequently need to refer to the code to study usage details.
    3: Good enough to make a quick start, without looking at the code.
    4: Documentation as one would expect for a well-known library.
