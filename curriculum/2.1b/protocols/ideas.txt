IP: some online interactive ITSM thingy?
TCP & UDP --> Servers & clients: Create something like IRC, with a text-based protocol.
Network protocols -> Protocols: SaxTP assignment
Crypt: modify the IRC assignment to include some encryption
RegExp: Write regexes for matching some data (online game?)
Parsers: Hand-implement a parser for your own language and implement some specified sample programs in it
--> Compilers: Compile the AST into Python code
Parser gens: Create your own language and implement the following programs in it (geared towards binary ops)
Protocol parser: implement an interpreter for a DSL for decoding network traffic (???)
    - checksum
    - retries
    - sequence number
    - 6 bits per color channel

