name: Aggregate functions
goals:
    aggregate: 1
resources:
    -
        link: https://www.youtube.com/watch?v=jcoJuc5e3RE
        title: Basic Aggregate Functions in SQL (COUNT, SUM, AVG, MAX, and MIN)
        info: Learn how to use SQL's basic aggregation functions like COUNT, SUM, AVG, MAX, and MIN in this step-by-step tutorial!
    -
        link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-group-by/
        title: PostgreSQL GROUP BY
        info: Practical examples of how to use `GROUP BY` and aggregate functions in PostgreSQL.
    -
        link: https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-having/
        title: "PostgreSQL Having"
        info: How the results of `GROUP BY` can be filtered using `HAVING`.
    -
        link: https://www.youtube.com/watch?v=nNrgRVIzeHg
        title: Advanced Aggregate Functions in SQL (GROUP BY, HAVING vs. WHERE)
        info: In this step-by-step video tutorial, you will learn some more advanced SQL aggregation concepts by using the GROUP BY, HAVING, and WHERE clauses!
assignment:
    Aggregate functions:
    - |
        In databases, data aggregation means that we apply an _aggregation function_ to rows to form a single value. Aggregation functions are built-in functions (in the database) that can perform some computation, like for example compute the sum of a list of numbers. Other aggregation functions are average, count, min and max.

        In other words we do not look at individual rows, but take many rows together and summarize data.

    - Study the database schema: |
        Because the database schema (which you can study using the `\d` command) is not so easy to comprehend, let's clarify the problem domain. In the following text, bold terms are table names.

        - A video **game** (such as *Street Fighter IV* and *The Sims: Unleashed*) has one **genre** (such as *Adventure*).
        - A **game** is published by one or more **publisher**s (such as *Epic Games* and *Activision*), connected through the **game_publisher** junction table.
        - A **game**s can be released for multiple **platform**s (such as *Wii*, *PS4* and *PC*). The release of a **game** for a specific **platform** (like the *PC* version of *Farcry*) is done by exactly one of the **game**'s **publisher**s. This connection is made through the **game_platform** junction table.
        - A **game** can be sold in multiple **region**s (such as *North America* and *Europe*). The **region_sales** table keeps track of how many thousands of copies of a **game** on a specific **platform** in a certain **region** have been sold.


    - SQL coding conventions: |
        When using aggregate functions, please write your queries in the following form:
            
        ```sql
        SELECT
            t.first_name,
            t.last_name,
            s.name,
            COUNT(p.id) AS count_projects
        FROM teacher t
        JOIN skill s ON s.teacher_id = t.id
        JOIN project p ON p.teacher_id = t.id
        WHERE t.level = 'Awesome'
            AND t.years_experience > 3
            AND s.name LIKE '%Programming%'
        GROUP BY t.teacher_id
        HAVING count_projects > 3
        ORDER BY t.first_name
        LIMIT 2
        ```

        So, in addition to the conventions mentioned in the previous assignment:
        * You should put the `GROUP BY` and `HAVING` clauses on a new line for clarity.
        * You may put `SELECT` parameters below each other for clarity, if the line would become very long otherwise.

    - Important notes: |
        - For each of the questions below, we expect your answer to be a *single* query, unless stated otherwise.
        - Your queries should not contain literal ids or other information that was not given in the question. So when the assignment says to show all info for *Frank*, you can't do something like `SELECT * FROM teachers WHERE id=42` but you need to do something like `SELECT * FROM teachers WHERE name='Frank'`.

    It's a GROUP effort:
    -
        ^merge: queries

    Mind if I JOIN you?:
    -
        ^merge: queries

    I will have what you are HAVING:
    -
        ^merge: queries
